import axios from '../../axios-api';
import {FETCH_NEWS_SUCCESS, FETCH_NEWS_FAILURE, FETCH_NEWS_REQUEST, FETCH_NES_SUCCESS} from "./Actions";




export const fetch_news_request = () =>{
  return{type: FETCH_NEWS_REQUEST}
};
export const fetch_news_success = news =>{
  return{type: FETCH_NEWS_SUCCESS, news}
};
export const fetch_news_failure = error =>{
  return{type: FETCH_NEWS_FAILURE, error}
};

export const fetch_nes_success = nes =>{
  return{type: FETCH_NES_SUCCESS, nes}
}

export const fetch_news = () =>{
  return dispatch =>{
    dispatch(fetch_news_request());
    axios.get('/news').then(response=>{
      dispatch(fetch_news_success(response.data))
    }, error=>{
      dispatch(fetch_news_failure(error))
    })
  }
};

export const fetch_news_id = id =>{
  return dispatch => {
    dispatch(fetch_news_request());
    axios.get('/news/' + id).then(res=>{
      dispatch(fetch_nes_success(res.data))
    },error=>{
      dispatch(fetch_news_failure(error))
    })
  }
};

export const post_news = data =>{
  return dispatch =>{
    dispatch(fetch_news_request());
    axios.post('/news', data).then(res=>{
      dispatch(fetch_news())
    },error=>{
      dispatch(fetch_news_failure(error))
    })
  }
};

export const delete_news = id =>{
  return dispatch =>{
    axios.delete('/news/'+ id).then(res=>{
      dispatch(fetch_news())
    })
  }
}