import {
  FETCH_COMMENT_FAILURE,
  FETCH_COMMENT_REQUEST,
  FETCH_COMMENT_SUCCESS, FETCH_NES_SUCCESS,
  FETCH_NEWS_FAILURE,
  FETCH_NEWS_REQUEST,
  FETCH_NEWS_SUCCESS
} from "./actions/Actions";

const initialState = {
  news: [],
  comment: [],
  nes: {},
  error: null
};

const reducer = (state = initialState, action)=>{
  switch (action.type) {

    case FETCH_NEWS_REQUEST:
      return{...state};
    case FETCH_NEWS_SUCCESS:
      return{...state, news: action.news};
    case FETCH_NEWS_FAILURE:
      return{...state, error: action.error};


    case FETCH_COMMENT_REQUEST:
      return{...state};
    case FETCH_COMMENT_SUCCESS:
      return{...state, comment: action.comment};
    case FETCH_COMMENT_FAILURE:
      return{...state, error: action.error};

    case FETCH_NES_SUCCESS:
      return{...state, nes: action.nes};

    default:
      return state;

  }
};

export default reducer;