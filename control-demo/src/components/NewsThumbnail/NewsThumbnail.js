import React from 'react';

import imageNotAvailable from '../../assets/images/image_not_available.jpeg';

const styles = {
  width: '100px',
  height: '100px',
  marginRight: '10px'
};


const NewsThumbnail = props => {
  let image = imageNotAvailable;

  if (props.image) {
    image = 'http://localhost:8000/uploads/' + props.image
  }

  if (props.image === 'null') {
    return null
  }



  return <img src={image} style={styles} className="img-thumbnail" alt="PostImage"/>
};

export default NewsThumbnail;