import React, {Component, Fragment} from 'react';
import {Link, NavLink as RouterLink} from "react-router-dom";
import {Button, Card, CardBody, NavLink} from 'reactstrap';
import {delete_news, fetch_news} from "../../store/actions/newsActions";
import {connect} from "react-redux";
import Moment from "react-moment";
import NewsThumbnail from "../../components/NewsThumbnail/NewsThumbnail";

class News extends Component {
  componentDidMount(){
    this.props.fetchNews()
  }
  render() {
    return (
      <Fragment>
        <h3>Posts</h3>
        <NavLink tag={RouterLink} to='/add'>Add new post</NavLink>
        {this.props.news.map(item=>(
          <Card key={item.id}>
            <CardBody>
             <NewsThumbnail image={item.image}/>
              <h4>{item.title}</h4>
              <hr/>
              <Moment format="DD.MM.YYYY HH:MM">
                {item.datatime}
              </Moment>
              <Link to={'/news/' + item.id}>
                Read full post  >>
              </Link>
              <Button color='danger' onClick={()=>this.props.delete_news(item.id)}>Delete</Button>
            </CardBody>
          </Card>
        ))}
      </Fragment>
    );
  }
}
const mapStateToProps = state =>({
    news: state.news
  });
const mapDispatchToProps = dispatch =>({
    fetchNews: ()=>dispatch(fetch_news()),
    delete_news: (id)=>dispatch(delete_news(id))
  });
export default connect(mapStateToProps, mapDispatchToProps) (News);