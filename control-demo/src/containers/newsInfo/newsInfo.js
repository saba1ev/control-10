import React, {Component} from 'react';
import {fetch_news_id} from "../../store/actions/newsActions";
import {connect} from "react-redux";
import NewsThumbnail from "../../components/NewsThumbnail/NewsThumbnail";

class NewsInfo extends Component {

  componentDidMount(){
    this.props.getOneNews(this.props.match.params.id)
  }
  render() {
    return (
      <div>
        <NewsThumbnail image={this.props.nes.image}/>
        <h1>{this.props.nes.title}</h1>
        <p><strong>Комент</strong></p>
        <p>{this.props.nes.description}</p>

      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    nes: state.nes,
    comment: state.comment
  }
};
const mapDispatchToProps = dispatch => {
  return {
    getOneNews: (id)=> dispatch(fetch_news_id(id))
  }
};

export default connect(mapStateToProps, mapDispatchToProps) (NewsInfo);