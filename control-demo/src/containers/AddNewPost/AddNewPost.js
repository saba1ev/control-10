import React, {Component} from 'react';
import {post_news} from "../../store/actions/newsActions";
import {Button, Form, FormGroup, Input, Label, NavLink} from "reactstrap";
import {connect} from "react-redux";
import {NavLink as routerLink} from "react-router-dom";

class AddNewPost extends Component {
  state = {
    title: '',
    description: '',
    image: null
  };
  submitHendler = (event) =>{
    event.preventDefault();

    const formData = new FormData();

    Object.keys(this.state).forEach(key => {
      formData.append(key, this.state[key])
    });

    this.props.postData(formData);
  };
  inputChangeHenlder = event =>{
    return this.setState({
      [event.target.name]: event.target.value
    })
  };
  fileChangeHendler = event =>{
    return this.setState({
      [event.target.name]: event.target.files[0]
    })
  };
  render() {
    return (
      <div>
        <h2>ADD NEW POST</h2>
        <Form>
          <FormGroup>
            <Label for="exampleEmail">Title</Label>
            <Input type="text"
                   name="title"
                   id="exampleEmail"
                   placeholder="Title"
                   value={this.state.title}
                   onChange={(event)=>this.inputChangeHenlder(event)}
            />
          </FormGroup>
          <FormGroup>
            <Label for="example">Content</Label>
            <Input type="textarea"
                   name="description"
                   id="example"
                   value={this.state.content}
                   onChange={(event)=>this.inputChangeHenlder(event)}
            />
          </FormGroup>
          <FormGroup>
            <Label for="examp">image</Label>
            <Input type="file"
                   name="image"
                   id="examp"
                   onChange={(event)=>this.fileChangeHendler(event)}
            />
          </FormGroup>
          <Button color='info' onClick={(event)=>this.submitHendler(event)}>
              <NavLink tag={routerLink} to='/'>save</NavLink>
          </Button>
        </Form>
      </div>
    );
  }
}
const mapDispatchToProps = dispatch =>{
  return{
    postData: (data)=>dispatch(post_news(data))
  }
};
export default connect(null, mapDispatchToProps) (AddNewPost);