import React, {Component, Fragment} from 'react';
import {Container, NavLink} from "reactstrap";
import {Switch, Route, NavLink as RouterLink} from 'react-router-dom'
import News from "./containers/News/News";
import AddNewPost from "./containers/AddNewPost/AddNewPost";
import newsInfo from "./containers/newsInfo/newsInfo";


class App extends Component {
  render() {
    return (
      <Fragment>
        <Container>
          <h1><NavLink tag={RouterLink} to='/'>News</NavLink></h1>
        </Container>
        <hr/>
        <Container style={{marginTop: '20px'}}>
          <Switch>
            <Route path='/news/:id' component={newsInfo}/>
            <Route path='/add' component={AddNewPost}/>
            <Route path='/' exact component={News}/>
          </Switch>
        </Container>
      </Fragment>
    );
  }
}

export default App;