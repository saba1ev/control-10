const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const config = require('../config');


const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const createRouter = connection => {


  const router = express.Router();


  router.get('/', (req, res) => {
    console.log("ss")
    connection.query('SELECT * FROM `news`', (error, results) => {
      console.log(error);
      if (error) {
        res.status(500).send({error: 'Database error'})
      }
      res.send(results)
    });

  });

  router.get('/:id', (req, res) => {
    connection.query('SELECT * FROM `news` WHERE `id` = ?', req.params.id, (error, results) => {
      if (error) {
        res.status(500).send({error: 'Database error'})
      }

      if (results[0]) {
        res.send(results[0]);
      } else {
        res.status(404).send({error: 'Item not found'});
      }

    });
  });

  router.post('/', upload.single('image'), (req, res) => {
    const news = req.body;
    const date = new Date().toISOString();

    if (req.file) {
      news.image = req.file.filename;
    }

    connection.query('INSERT INTO `news` (`id`, `title`, `description`, `image`, `datetime`) VALUES (?, ?, ?, ?, ?)',
      [news.id, news.title, news.description, news.image, date],
      (error, results) => {
        if (error) {
          res.status(500).send({error: 'Database error'})
        }
        res.send({message: 'OK'})
      }
    );
  });


  router.put('/:id', (req, res) => {
    const item = req.body;
    connection.query('UPDATE `news` SET `title` = "' + item.title
      + '", `description` = "' + item.description + '" WHERE id = ?', req.params.id, (error, results) => {
      if (error) {
        res.status(500).send({error: 'Database error'});
      }
      res.send({message: 'OK'});
    });
  });

  router.delete('/:id', (req, res) => {

    connection.query('DELETE FROM `news` WHERE `id` = ?', req.params.id, (error, results) => {

      if (error) {
        console.log(error);
        res.status(500).send({error: 'Database error'});
      }
      res.send({message: 'Delete ok'});
    });

  });


  return router

};


module.exports = createRouter;
