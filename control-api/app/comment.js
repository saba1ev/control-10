const express = require('express');


const createRouter = connection => {

  const router = express.Router();


  router.get('/', (req, res) => {
    connection.query('SELECT * FROM `comment`', (error, results) => {
      if (error) {
        res.status(500).send({error: 'Database error'})
      }
      res.send(results);
    });

  });


  router.get('/:id', (req, res) => {
    connection.query('SELECT * FROM `comment` WHERE `id` = ?', req.params.id, (error, results) => {
      if (error) {
        res.status(500).send({error: 'Database error'})
      }

      if (results[0]) {
        res.send(results[0]);
      } else {
        res.status(404).send({error: 'Item not found'});
      }

    });
  });

  router.post('/', (req, res) => {
    const comment = req.body;
    let author = comment.author;
    if (comment.author === ''){
      author = 'Anonymous'
    }

    connection.query('INSERT INTO `comment` (`id`,`news_id` ,`author` ,`comment`) VALUES (?, ?, ?, ?)',
      [comment.id, comment.news_id, author, comment.comment],
      (error, results) => {
        if (error) {
          res.status(500).send({error: 'Database error'})
        }
        res.send({message: 'OK'})
      }
    );
  });

  router.delete('/:id', (req, res) => {
    connection.query('DELETE FROM `comment` WHERE `id` = ?', req.params.id, (error, results) => {

      if (error) {
        res.status(500).send({error: 'Database error'});
      }
      res.send({message: 'Delete ok'});
    });

  });

  return router
};


module.exports = createRouter;