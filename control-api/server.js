const express = require('express');
const cors = require('cors');
const mysql = require('mysql');
const news = require('./app/news');
const comment = require('./app/comment');



const app = express();
app.use(cors());
app.use(express.json());
app.use(express.static('public'));


const port = 8000;


const connection = mysql.createConnection({
  host: 'localhost',
  user: 'user',
  password: '1234',
  database: 'db-sobolev'
});


app.use('/news', news(connection));
app.use('/comment', comment(connection));



connection.connect((err) => {
  if (err) {
    console.error('error connecting: ' + err.stack);
    return;
  }

  console.log('connected as id ' + connection.threadId);

  app.listen(port, () => {
    console.log(`Server started on ${port} port`);
  });
});
