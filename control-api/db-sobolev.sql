CREATE SCHEMA `db-sobolev` DEFAULT CHARACTER SET utf8 ;

USE `db-sobolev` ;

CREATE TABLE `news` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(45) NOT NULL,
  `description` TEXT NOT NULL,
  `image` VARCHAR(100) NULL,
  `datetime` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`));
CREATE TABLE `comment` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `news_id` INT NOT NULL,
  `author` VARCHAR(255) NULL,
  `comment` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `news_id_fk_idx` (`news_id` ASC),
  CONSTRAINT `news_id_fk`
    FOREIGN KEY (`news_id`)
    REFERENCES `news` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION);